var AdmZip = require('adm-zip'); //Making this process much easier to do

//Require some stuff that we need

const fs = require('fs');
const mkdirp = require('mkdirp');
const readline = require('readline');
const path = require('path');
const FileHound = require('filehound');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

//----------------------------------

//A bunch of vars that I will forget about

var arg = process.argv[2];

const files = FileHound.create()
  .paths('./unpacked')
  .ext('jar')
  .find();

//----------------------------------

function stop() { //This is how lazy I can be, but this makes closing out the program easy to do.
    console.log('Stopping process...');
    process.exit();
}

function pack() //This is what tries to pack all the mods into the correct folders and zips
{
    files.then(files => {
        files.forEach(file => {
          if(file.replace('unpacked\\', '').replace('unpacked/', '') != "modpack.jar")
          {
            var modname = file.replace('unpacked\\', '').replace('unpacked/', '');
            var modslug = modname.replace('.jar', '').toLowerCase().split(/[_-]/g)[0];
            var output = 'packed/' + modslug + '/' + modname.replace('.jar', '').toLowerCase() + '.zip';
            console.log('      Found mod:', modname);
            console.log('        Packed and saved to "' + output + '"');
            mkdirp('packed/' + modslug, function (err) {
                if (err) console.error(err)
            });
            var zip = new AdmZip();
            zip.addLocalFile(file, 'mods/');
            zip.writeZip(output);
          } else {
            var modname = file.replace('unpacked\\', '').replace('unpacked/', '');
            var modslug = modname.replace('.jar', '').replace('.', '').toLowerCase().split(/[_-]/g)[0];
            var output = 'packed/' + modslug + '/' + modname.replace('.jar', '').toLowerCase() + '.zip';
            console.log('      Found mod:', modname);
            console.log('        Packed and saved to "' + output + '"');
            mkdirp('packed/' + modslug, function (err) {
                if (err) console.error(err)
            });
            var zip = new AdmZip();
            zip.addLocalFile(file, 'bin/');
            zip.writeZip(output);
          }
        });
        stop();
    });
}

function unpack() //Prob the worst thing i'm ever going to develop
{

}

function initpack() //Init the pack function by checking if the folder actually exist or not
{
    console.log('Checking if the "unpacked" exist...'); //Very important step, as if a folder didn't exist this whole thing would break
    if (fs.existsSync('./unpacked'))
    {
        console.log('Folder found. Checking contents...');
        fs.readdir('./unpacked', (err, files) => { //Also important as we need files in order to pack them
            console.log('    Files found: ' + files.length + ' mods');
            if(files.length == 0)
            {
                console.log('Folder is empty, therefore nothing can be done. Add the mods to the folder and then run this command again.');
                stop(); //Call apon my lazy code to close out the node process
            } else {
                pack(); //We finally can start to pack the items
            }
        })
    } else {
        console.log('Folder not found. Please create it manually or run the setup command.')
        stop(); //Call apon my lazy code to close out the node process
    }
}

function initunpack() //Init the unpack function by checking if the folder actually exist or not
{
    console.log('Checking if the "packed" exist...');
    if (fs.existsSync('./packed'))
    {
        console.log('Folder found. Checking contents...');
    } else {
        console.log('Folder not found. Please create it manually or run the setup command.')
        stop();
    }
}

if(arg == 'help' || arg == null) //Here for anyone who wants to use it for some reason
{
    console.log('\nHere is a list of args you can use:\n');
        console.log('   help   = This command here');
        console.log('   setup  = Create folders needed for the program to function (Run this first and only once)'); //Very important
        console.log('   pack   = Pack mods inside the "unpacked" folder into the solder file structure');
        console.log('   unpack = Unpack all mods from the solder file structure into their original state (Not done)\n');
    process.exit(); //This is for people who run npm run test
}

if(arg == 'setup') //A faster way to create 2 folders, beacause some people are just lazy
{
    console.log('Doing setup. This most likely will take less then a second.');
    if (fs.existsSync('./packed'))
    { //Check if the folder exist so we don't run into problems
        console.log('   The "packed" folder exist already.')
    } else {
        mkdirp('./packed', function (err) {
            if (err) console.error(err)
        });
    }
    if (fs.existsSync('./unpacked'))
    { //Check if the folder exist so we don't run into problems
        console.log('   The "unpacked" folder exist already.')
    } else {
        mkdirp('./unpacked', function (err) {
            if (err) console.error(err)
        });
    }
    stop(); //Call apon my lazy code to close out the node process
}

if(arg == 'pack')
{
    //We make sure we tell people that this thing may not work as intended because of mod devs and the fact they name shit weird
    console.log(
    '*Warning*\n' +
    'This command may be unstable in a sense that minecraft mod names are not that universal.\n' +
    'Please understand that you will still have to check and make sure that the files where created correctly.\n' +
    'This is really just ment for making the process of putting the items together easy.\n' +
    '\nIf you want to pack forge, put a jar file called modpack.jar into the unpacked folder and it will pack it for you.\n' +
    '(Note you can only pack one version of forge like this)\n');

    //Make sure that you are mentaly prepared for using this pile of crap that I call a program
    console.log('Are you sure you would like to continue. [y/n]');
    rl.on('line', (input) => { //Wait for an input
        if(input.toLowerCase() == 'y')
        {
            console.log(`\nStarting packing process now...`);
            rl.close(); //Close out the input stream
            initpack() //Init the packing process
        } else if(input.toLowerCase() == 'n')
        {
            stop(); //Call apon my lazy code to close out the node process
        } else {
            console.log('Invalid input, Please try again.'); //I don't close the input stream, so it still waits for a y or n answer
        }
    });
}
